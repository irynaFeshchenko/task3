import movie1 from '../assets/images/movies/movie1.jpg';
import movie2 from '../assets/images/movies/movie2.jpg';
import movie3 from '../assets/images/movies/movie3.jpg';
import movie4 from '../assets/images/movies/movie4.jpg';
import movie5 from '../assets/images/movies/movie5.jpg';
import movie6 from '../assets/images/movies/movie6.jpg';
export const mockMovies = [
  {
    id: "1",
    name: "Pulp Fiction",
    yearOfRelise: "2004",
    genre: ["Action & Adventure"],
    poster: movie1
  },
  {
    id: "2",
    name: "Bohemian Rhapsody",
    yearOfRelise: "2003",
    genre: ["Drama", "Biography", "Music"],
    poster: movie2
  },
  {
    id: "3",
    name: "Kill Bill: Vol 2",
    yearOfRelise: "1994",
    genre: ["Oscar winning Movie"],
    poster: movie3
  },
  {
    id: "4",
    name: "Avengers: War of Infinity",
    yearOfRelise: "2004",
    genre: ["Action & Adventure"],
    poster: movie4
  },
  {
    id: "5",
    name: "Inception",
    yearOfRelise: "2003",
    genre: ["Action & Adventure"],
    poster: movie5
  },
  {
    id: "6",
    name: "Reservoir dogs",
    yearOfRelise: "1994",
    genre: ["Oscar winning Movie"],
    poster: movie6
  },
];
