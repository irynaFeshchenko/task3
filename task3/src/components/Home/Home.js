import "./Home.scss";
import HomeMenu from "./Menu/Menu";
import MoviesList from './MoviesList/MoviesList';

function Home() {
  return (
    <main>
      <HomeMenu></HomeMenu>
      <MoviesList></MoviesList>
    </main>
  );
}

export default Home;
