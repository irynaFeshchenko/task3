import "./MovieCard.scss";
import React from "react";

function MovieCard({ item }) {
  return (
    <div className="card">
      <img src={item.poster} alt="" />
      <div className="info-movie">
        <div className="info">
        <p className="info-movie_name">{item.name}</p>
        <p className="info-movie_genre">{item.genre.join(', ')}</p>
        </div>
        <p className="info-movie_realize-date">{item.yearOfRelise}</p>
      </div>
    </div>
  );
}

export default MovieCard;
