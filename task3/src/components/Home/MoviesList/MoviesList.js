import "./MoviesList.scss";
import React from "react";
import MovieCard from "../MovieCard/MovieCard";
import ErrorBoundary from "../../ErrorBoundary/ErrorBoundary";
import { mockMovies } from "../../../constants/mockMovies";

function MoviesList() {
  const items = mockMovies;
  return (
    <div className="main-list">
      <p className="movies-count">{items.length} movies found</p>
      <div className="movies-list">
        <ErrorBoundary>
          {items.map((item) => (
            <React.Fragment key={item.id}>
              <MovieCard item={item}></MovieCard>
            </React.Fragment>
          ))}
        </ErrorBoundary>
      </div>
    </div>
  );
}

export default MoviesList;
