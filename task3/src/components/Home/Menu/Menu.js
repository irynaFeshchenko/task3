import "./Menu.scss";

function HomeMenu() {
  return (
    <div className="home-menu">
      <div className="home-menu_types">
        <a href="#all" className="active">
          ALL
        </a>
        <a href="#documentary">DOCUMENTARY</a>
        <a href="#comedy">COMEDY</a>
        <a href="#horror">HORROR</a>
        <a href="#crime">CRIME</a>
      </div>
      <div className="home-menu_adittion-types">
        <div className="dropdown">
          <div className="hover">
            <span>SORT BY</span>
          </div>
          <div className="list">
            <a href="#">Decorazza Velours</a>
          </div>
        </div>
        <div className="dropdown">
          <div className="hover">
            <span>RELEASE DATE</span>
          </div>
          <div className="list">
            <a href="#">Decorazza Velours</a>
            <a href="#">Decorazza Seta</a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomeMenu;
