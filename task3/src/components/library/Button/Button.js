import { ColorsButtons } from "../../../constants/colorsButtons.enum";
import "./Button.scss";

function Button({ textButton, colorButton }) {
  function checkColor() {
    if (colorButton === ColorsButtons.PinkButton) {
      return "pink-button";
    } else {
      return "dark-button";
    }
  }
  return <div className="container-button"><button className={checkColor()}>{textButton}</button></div>;
}

export default Button;
