import "./Header.scss";
import { ReactComponent as NetflixLogo } from "../../assets/images/netflix.svg";
import Button from "../library/Button/Button";
import { ColorsButtons } from "../../constants/colorsButtons.enum";

function Header() {
  return (
    <header>
      <div className="container">
        <div className="header_menu">
          <NetflixLogo />
          <Button
            textButton={"+ ADD MOVIE"}
            colorButton={ColorsButtons.DarkButton}
          ></Button>
        </div>
        <div className="header_search">
          <p className="header_search_title">FIND YOUR MOVIE</p>
          <form className="search_form">
            <input
              type="text"
              name="name"
              placeholder="What do you want to watch?"
            />
            <Button
              textButton={"SEARCH"}
              colorButton={ColorsButtons.PinkButton}
            ></Button>
          </form>
        </div>
      </div>
    </header>
  );
}

export default Header;
