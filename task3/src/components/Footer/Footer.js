import { ReactComponent as NetflixLogo } from "../../assets/images/netflix.svg";
import './Footer.scss';

function Footer() {
  return (
    <footer>
        <NetflixLogo/>
    </footer>
  );
}

export default Footer;
